# Homesearch Laravel pizzeria test

This test is for PHP developer candidates to demonstrate their familiarity with Laravel, SOLID principles, TDD
and databases.

This test is a scenario where you're presented with an incomplete code base that was created in a rush by someone not
very familiar with Laravel.

To keep the test simpler, don't concern yourself with access control, but do outline how you would go about enforcing it.

## Running stuff

### Docker
As a courtesy, a docker-compose file is provided as a nearly ready to use environment that's only missing SSH keys.
You'll also need to create an environment file for the application and the Docker container.
Sample files are provided as `.env.example` for both cases.

They should work without requiring any changes once copied into place except for
`PHP_FILES_LOCATION` in the Docker environment file, which you need to set to
the directory where you cloned the test code or its direct parent.

### The rest of it
Once you have your environment files in place:
- Create an application key using:
    ```shell
    php artisan key:generate
    ```
- Install dependencies using composer.
- If you'd like to use the Docker compose file provided, run:
    ```shell
    docker-compose up -d
    ```
  in the `resources/docker/` directory.
- To get into the PHP container, run:
    ```shell
    docker exec -it dev_test_php_fpm bash
    ```
### Tests
There's a rudimentary endpoint test provided that you can run to test whether things work for you locally.
You can run tests using e.g.:
```shell
vendor/bin/phpunit --testdox --verbose
```

## Goals

Bonus goals are nice to have, but only do them if there's any time left.

### Minimum
- Refactor the codebase so there are models in place and used to return data.
- Add functionality for deleting ingredients.
- Add tests for any newly added functionality.

### Bonus
- Improve existing tests.
- Add support for ordering products that aren't pizza, e.g. drinks.
- Add support for creating custom pizzas.
- Add support for showing orders.
