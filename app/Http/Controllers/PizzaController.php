<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\Database\Seeders\PizzaSeeder;

class PizzaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): array
    {
        return PizzaSeeder::PIZZAS;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        throw new Exception('Not implemented yet');
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): array
    {
        $pizzas = collect(PizzaSeeder::PIZZAS);
        $pizza = $pizzas->where('id', $id)->first();

        if (empty($pizza)) {
            throw new NotFoundHttpException('Could not find a pizza with the ID provided.');
        }

        return $pizza;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        throw new Exception('Not implemented yet');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        throw new Exception('Not implemented yet');
    }
}
