<?php

use Illuminate\Database\{
    Migrations\Migration,
    Schema\Blueprint,
};
use Illuminate\Support\Facades\Schema;

class CreatePizzaIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pizza_ingredients', function (Blueprint $table) {
            $table->unsignedSmallInteger('pizza_id');
            $table->unsignedSmallInteger('ingredient_id');
            $table->timestamps();

            $table->foreign('pizza_id')
                ->references('id')
                ->on('pizzas');

            $table->foreign('ingredient_id')
                ->references('id')
                ->on('ingredients');

            $table->unique(['pizza_id', 'ingredient_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pizza_ingredients');
    }
}
