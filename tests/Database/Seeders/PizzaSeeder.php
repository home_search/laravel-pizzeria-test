<?php

namespace Tests\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PizzaSeeder extends Seeder
{
    public const PIZZAS = [[
        'id' => 1,
        'name' => 'Peanut dream',
        'description' => 'Lavish, with our tomato sauce base, mozzarella and roasted peanuts.',
    ], [
        'id' => 2,
        'name' => 'Margherita',
        'description' => 'A classic tomato sauce and mozzarella experience.',
    ]];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pizzas')->insertOrIgnore(static::PIZZAS);
    }
}
