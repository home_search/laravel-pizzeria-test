<?php

namespace Tests;

use Illuminate\Foundation\Testing\{
    RefreshDatabase,
    TestCase as BaseTestCase,
};

class DatabaseTestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    protected array $seederClasses = [];

    protected function setUp(): void
    {
        parent::setUp();
        $this->runAllSeeders();
    }

    protected function runAllSeeders(): void
    {
        foreach ($this->seederClasses as $seeder) {
            $this->runSeeder($seeder);
        }
    }

    protected function runSeeder(string $class): void
    {
        $this->artisan('db:seed', [
            '--class' => $class,
        ]);
    }
}
