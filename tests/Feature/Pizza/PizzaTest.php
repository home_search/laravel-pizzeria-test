<?php

namespace Tests\Feature\Pizza;

use Illuminate\Http\Response;
use Tests\Database\Seeders\PizzaSeeder;
use Tests\DatabaseTestCase;

class PizzaTest extends DatabaseTestCase
{
    protected string $endPoint = '/api/pizza';
    protected array $seederClasses = [
        PizzaSeeder::class,
    ];

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPizzaList()
    {
        $this->get($this->endPoint)
            ->assertOk()
            ->assertExactJson(PizzaSeeder::PIZZAS);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPizzaFound()
    {
        $this->endPoint .= '/1';
        $this->get($this->endPoint)
            ->assertOk()
            ->assertExactJson(PizzaSeeder::PIZZAS[0]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPizzaNotFound()
    {
        $this->endPoint .= '/3';
        $this->get($this->endPoint)
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
